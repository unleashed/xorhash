#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet
# at http://www.illumos.org/license/CDDL.
#

#
# Copyright 2011, Richard Lowe
# Copyright 2012 Nexenta Systems, Inc. All rights reserved.
#

file path=usr/share/man/man3bsm/au_open.3bsm
file path=usr/share/man/man3bsm/au_preselect.3bsm
file path=usr/share/man/man3bsm/au_to.3bsm
file path=usr/share/man/man3bsm/au_user_mask.3bsm
file path=usr/share/man/man3bsm/getauclassent.3bsm
file path=usr/share/man/man3bsm/getauditflags.3bsm
file path=usr/share/man/man3bsm/getauevent.3bsm
file path=usr/share/man/man3bsm/getfauditflags.3bsm
