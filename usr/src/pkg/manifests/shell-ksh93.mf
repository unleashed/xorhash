# Copyright 2017 Alexander Eremin <alexander.r.eremin@gmail.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
set name=pkg.fmri \
    value=pkg:/shell/ksh93@93.20100309,$(PKGVERS_BUILTON)$(PKGVERS_BRANCH)
set name=pkg.description \
    value="Korn Shell 93: A command and programming language that executes commands read from a terminal or a file"
set name=pkg.summary value="Ksh93 - The AT&T Korn Shell"
set name=info.upstream \
    value="AT&T Software Technology (AST) OpenSource Software Collection"
set name=info.upstream-url value=http://www2.research.att.com/sw/download/
dir path=usr/include/ast
dir path=usr/lib
dir path=usr/lib/$(ARCH64)
dir path=usr/share/doc group=other
dir path=usr/share/doc/ksh
dir path=usr/share/doc/ksh/images
dir path=usr/share/doc/ksh/images/callouts
dir path=usr/share/man/man1
file path=etc/ksh.kshrc group=sys preserve=renameold
file path=etc/skel/.kshrc group=other preserve=true
file path=usr/bin/$(ARCH32)/ksh93 mode=0555
file path=usr/bin/$(ARCH64)/ksh93 mode=0555
file path=usr/bin/alias mode=0555
file path=usr/bin/shcomp mode=0555
file path=usr/include/ast/align.h
file path=usr/include/ast/ast.h
file path=usr/include/ast/ast_botch.h
file path=usr/include/ast/ast_ccode.h
file path=usr/include/ast/ast_common.h
file path=usr/include/ast/ast_dir.h
file path=usr/include/ast/ast_dirent.h
file path=usr/include/ast/ast_fcntl.h
file path=usr/include/ast/ast_float.h
file path=usr/include/ast/ast_fs.h
file path=usr/include/ast/ast_getopt.h
file path=usr/include/ast/ast_iconv.h
file path=usr/include/ast/ast_lib.h
file path=usr/include/ast/ast_limits.h
file path=usr/include/ast/ast_map.h
file path=usr/include/ast/ast_mmap.h
file path=usr/include/ast/ast_mode.h
file path=usr/include/ast/ast_namval.h
file path=usr/include/ast/ast_ndbm.h
file path=usr/include/ast/ast_nl_types.h
file path=usr/include/ast/ast_param.h
file path=usr/include/ast/ast_standards.h
file path=usr/include/ast/ast_std.h
file path=usr/include/ast/ast_stdio.h
file path=usr/include/ast/ast_sys.h
file path=usr/include/ast/ast_time.h
file path=usr/include/ast/ast_tty.h
file path=usr/include/ast/ast_version.h
file path=usr/include/ast/ast_vfork.h
file path=usr/include/ast/ast_wait.h
file path=usr/include/ast/ast_wchar.h
file path=usr/include/ast/ast_windows.h
file path=usr/include/ast/bytesex.h
file path=usr/include/ast/ccode.h
file path=usr/include/ast/cdt.h
file path=usr/include/ast/cmd.h
file path=usr/include/ast/cmdext.h
file path=usr/include/ast/debug.h
file path=usr/include/ast/dirent.h
file path=usr/include/ast/dlldefs.h
file path=usr/include/ast/dt.h
file path=usr/include/ast/endian.h
file path=usr/include/ast/error.h
file path=usr/include/ast/find.h
file path=usr/include/ast/fnmatch.h
file path=usr/include/ast/fnv.h
file path=usr/include/ast/fs3d.h
file path=usr/include/ast/fts.h
file path=usr/include/ast/ftw.h
file path=usr/include/ast/ftwalk.h
file path=usr/include/ast/getopt.h
file path=usr/include/ast/glob.h
file path=usr/include/ast/hash.h
file path=usr/include/ast/hashkey.h
file path=usr/include/ast/hashpart.h
file path=usr/include/ast/history.h
file path=usr/include/ast/iconv.h
file path=usr/include/ast/ip6.h
file path=usr/include/ast/lc.h
file path=usr/include/ast/ls.h
file path=usr/include/ast/magic.h
file path=usr/include/ast/magicid.h
file path=usr/include/ast/mc.h
file path=usr/include/ast/mime.h
file path=usr/include/ast/mnt.h
file path=usr/include/ast/modecanon.h
file path=usr/include/ast/modex.h
file path=usr/include/ast/namval.h
file path=usr/include/ast/nl_types.h
file path=usr/include/ast/nval.h
file path=usr/include/ast/option.h
file path=usr/include/ast/preroot.h
file path=usr/include/ast/proc.h
file path=usr/include/ast/prototyped.h
file path=usr/include/ast/re_comp.h
file path=usr/include/ast/recfmt.h
file path=usr/include/ast/regex.h
file path=usr/include/ast/regexp.h
file path=usr/include/ast/sfdisc.h
file path=usr/include/ast/sfio.h
file path=usr/include/ast/sfio_s.h
file path=usr/include/ast/sfio_t.h
file path=usr/include/ast/shcmd.h
file path=usr/include/ast/shell.h
file path=usr/include/ast/sig.h
file path=usr/include/ast/stack.h
file path=usr/include/ast/stak.h
file path=usr/include/ast/stdio.h
file path=usr/include/ast/stk.h
file path=usr/include/ast/sum.h
file path=usr/include/ast/swap.h
file path=usr/include/ast/tar.h
file path=usr/include/ast/times.h
file path=usr/include/ast/tm.h
file path=usr/include/ast/tmx.h
file path=usr/include/ast/tok.h
file path=usr/include/ast/tv.h
file path=usr/include/ast/usage.h
file path=usr/include/ast/vdb.h
file path=usr/include/ast/vecargs.h
file path=usr/include/ast/vmalloc.h
file path=usr/include/ast/wait.h
file path=usr/include/ast/wchar.h
file path=usr/include/ast/wordexp.h
file path=usr/lib/$(ARCH64)/libast.so.1
file path=usr/lib/$(ARCH64)/libcmd.so.1
file path=usr/lib/$(ARCH64)/libdll.so.1
file path=usr/lib/$(ARCH64)/libshell.so.1
file path=usr/lib/$(ARCH64)/libsum.so.1
file path=usr/lib/libast.so.1
file path=usr/lib/libcmd.so.1
file path=usr/lib/libdll.so.1
file path=usr/lib/libshell.so.1
file path=usr/lib/libsum.so.1
file path=usr/share/doc/ksh/COMPATIBILITY
file path=usr/share/doc/ksh/DESIGN
file path=usr/share/doc/ksh/OBSOLETE
file path=usr/share/doc/ksh/README
file path=usr/share/doc/ksh/RELEASE
file path=usr/share/doc/ksh/TYPES
file path=usr/share/doc/ksh/images/callouts/1.png
file path=usr/share/doc/ksh/images/callouts/10.png
file path=usr/share/doc/ksh/images/callouts/2.png
file path=usr/share/doc/ksh/images/callouts/3.png
file path=usr/share/doc/ksh/images/callouts/4.png
file path=usr/share/doc/ksh/images/callouts/5.png
file path=usr/share/doc/ksh/images/callouts/6.png
file path=usr/share/doc/ksh/images/callouts/7.png
file path=usr/share/doc/ksh/images/callouts/8.png
file path=usr/share/doc/ksh/images/callouts/9.png
file path=usr/share/doc/ksh/images/tag_bourne.png
file path=usr/share/doc/ksh/images/tag_i18n.png
file path=usr/share/doc/ksh/images/tag_ksh.png
file path=usr/share/doc/ksh/images/tag_ksh88.png
file path=usr/share/doc/ksh/images/tag_ksh93.png
file path=usr/share/doc/ksh/images/tag_l10n.png
file path=usr/share/doc/ksh/images/tag_perf.png
file path=usr/share/doc/ksh/shell_styleguide.docbook
file path=usr/share/man/man1/alias.1
file path=usr/share/man/man1/ksh93.1
file path=usr/share/man/man1/shcomp.1
hardlink path=usr/bin/$(ARCH32)/ksh target=ksh93
hardlink path=usr/bin/$(ARCH32)/rksh target=ksh93
hardlink path=usr/bin/$(ARCH32)/rksh93 target=ksh93
hardlink path=usr/bin/$(ARCH64)/ksh target=ksh93
hardlink path=usr/bin/$(ARCH64)/rksh target=ksh93
hardlink path=usr/bin/$(ARCH64)/rksh93 target=ksh93
hardlink path=usr/bin/bg target=../../usr/bin/alias
hardlink path=usr/bin/cd target=../../usr/bin/alias
hardlink path=usr/bin/cksum target=../../usr/bin/alias
hardlink path=usr/bin/cmp target=../../usr/bin/alias
hardlink path=usr/bin/comm target=../../usr/bin/alias
hardlink path=usr/bin/command target=../../usr/bin/alias
hardlink path=usr/bin/cut target=../../usr/bin/alias
hardlink path=usr/bin/fc target=../../usr/bin/alias
hardlink path=usr/bin/fg target=../../usr/bin/alias
hardlink path=usr/bin/getopts target=../../usr/bin/alias
hardlink path=usr/bin/hash target=../../usr/bin/alias
hardlink path=usr/bin/jobs target=../../usr/bin/alias
hardlink path=usr/bin/join target=../../usr/bin/alias
hardlink path=usr/bin/kill target=../../usr/bin/alias
hardlink path=usr/bin/ksh target=../../usr/lib/isaexec
hardlink path=usr/bin/ksh93 target=../../usr/lib/isaexec
hardlink path=usr/bin/logname target=../../usr/bin/alias
hardlink path=usr/bin/paste target=../../usr/bin/alias
hardlink path=usr/bin/pfksh target=../../usr/bin/pfexec
hardlink path=usr/bin/pfksh93 target=../../usr/bin/pfexec
hardlink path=usr/bin/pfrksh target=../../usr/bin/pfexec
hardlink path=usr/bin/pfrksh93 target=../../usr/bin/pfexec
hardlink path=usr/bin/print target=../../usr/bin/alias
hardlink path=usr/bin/read target=../../usr/bin/alias
hardlink path=usr/bin/rev target=../../usr/bin/alias
hardlink path=usr/bin/rksh target=../../usr/lib/isaexec
hardlink path=usr/bin/rksh93 target=../../usr/lib/isaexec
hardlink path=usr/bin/sleep target=../../usr/bin/alias
hardlink path=usr/bin/sum target=../../usr/bin/alias
hardlink path=usr/bin/tee target=../../usr/bin/alias
hardlink path=usr/bin/test target=../../usr/bin/alias
hardlink path=usr/bin/type target=../../usr/bin/alias
hardlink path=usr/bin/ulimit target=../../usr/bin/alias
hardlink path=usr/bin/umask target=../../usr/bin/alias
hardlink path=usr/bin/unalias target=../../usr/bin/alias
hardlink path=usr/bin/uniq target=../../usr/bin/alias
hardlink path=usr/bin/wait target=../../usr/bin/alias
hardlink path=usr/bin/wc target=../../usr/bin/alias
license usr/src/lib/libast/THIRDPARTYLICENSE \
    license=usr/src/lib/libast/THIRDPARTYLICENSE
license usr/src/lib/libcmd/THIRDPARTYLICENSE \
    license=usr/src/lib/libcmd/THIRDPARTYLICENSE
license usr/src/lib/libdll/THIRDPARTYLICENSE \
    license=usr/src/lib/libdll/THIRDPARTYLICENSE
license usr/src/lib/libshell/THIRDPARTYLICENSE \
    license=usr/src/lib/libshell/THIRDPARTYLICENSE
license usr/src/lib/libsum/THIRDPARTYLICENSE \
    license=usr/src/lib/libsum/THIRDPARTYLICENSE
link path=sbin/jsh target=../usr/bin/ksh93
link path=sbin/sh target=../usr/bin/$(ARCH32)/ksh93
link path=usr/bin/jsh target=ksh93
link path=usr/bin/sh target=$(ARCH32)/ksh93
link path=usr/lib/$(ARCH64)/libcmd.so target=libcmd.so.1
link path=usr/lib/libcmd.so target=libcmd.so.1
link path=usr/lib/rsh target=../bin/ksh93
