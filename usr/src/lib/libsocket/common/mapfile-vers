#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#
#
# Copyright (c) 2006, 2010, Oracle and/or its affiliates. All rights reserved.
#
# Copyright (c) 2013, OmniTI Computer Consulting, Inc. All rights reserved.

#
# MAPFILE HEADER START
#
# WARNING:  STOP NOW.  DO NOT MODIFY THIS FILE.
# Object versioning must comply with the rules detailed in
#
#	usr/src/lib/README.mapfiles
#
# You should not be making modifications here until you've read the most current
# copy of that file. If you need help, contact a gatekeeper for guidance.
#
# MAPFILE HEADER END
#

$mapfile_version 2

SYMBOL_VERSION ILLUMOS_0.2 {	# reentrant ethers(3SOCKET)
    global:
	ether_aton_r	{ TYPE = FUNCTION };
	ether_ntoa_r	{ TYPE = FUNCTION };
} ILLUMOS_0.1;

SYMBOL_VERSION ILLUMOS_0.1 {    # Illumos additions
    global:
        accept4	{ TYPE = FUNCTION };
} SUNW_1.7;

SYMBOL_VERSION SUNW_1.7 {
    global:
	freeifaddrs	{ TYPE = FUNCTION };
	getifaddrs	{ TYPE = FUNCTION };
} SUNW_1.6;

SYMBOL_VERSION SUNW_1.6 {
    global:
	getipv4sourcefilter	{ TYPE = FUNCTION };
	getsourcefilter	{ TYPE = FUNCTION };
	setipv4sourcefilter	{ TYPE = FUNCTION };
	setsourcefilter	{ TYPE = FUNCTION };
} SUNW_1.5;

SYMBOL_VERSION SUNW_1.5 {
    global:
	inet6_opt_append	{ TYPE = FUNCTION };
	inet6_opt_find	{ TYPE = FUNCTION };
	inet6_opt_finish	{ TYPE = FUNCTION };
	inet6_opt_get_val	{ TYPE = FUNCTION };
	inet6_opt_init	{ TYPE = FUNCTION };
	inet6_opt_next	{ TYPE = FUNCTION };
	inet6_opt_set_val	{ TYPE = FUNCTION };
	inet6_rth_add	{ TYPE = FUNCTION };
	inet6_rth_getaddr	{ TYPE = FUNCTION };
	inet6_rth_init	{ TYPE = FUNCTION };
	inet6_rth_reverse	{ TYPE = FUNCTION };
	inet6_rth_segments	{ TYPE = FUNCTION };
	inet6_rth_space	{ TYPE = FUNCTION };
	sockatmark	{ TYPE = FUNCTION };
	__xnet_getaddrinfo	{ TYPE = FUNCTION };
} SUNW_1.4;

SYMBOL_VERSION SUNW_1.4 {
    global:
	freeaddrinfo	{ TYPE = FUNCTION };
	gai_strerror	{ TYPE = FUNCTION };
	getaddrinfo	{ TYPE = FUNCTION };
	getnameinfo	{ TYPE = FUNCTION };
	if_freenameindex	{ TYPE = FUNCTION };
	if_indextoname	{ TYPE = FUNCTION };
	if_nameindex	{ TYPE = FUNCTION };
	if_nametoindex	{ TYPE = FUNCTION };
	in6addr_any	{ TYPE = DATA; SIZE = 0x10 };
	in6addr_loopback	{ TYPE = DATA; SIZE = 0x10 };
} SUNW_1.3;

# Due to mistakes made early in the history of this library, there are
# no SUNW_1.2 or SUNW_1.3 symbols, but they are now kept as placeholders.
# Don't add any symbols to these versions.

SYMBOL_VERSION SUNW_1.3 {
    global:
	SUNW_1.3;
} SUNW_1.2;

SYMBOL_VERSION SUNW_1.2 {
    global:
	SUNW_1.2;
} SUNW_1.1;

SYMBOL_VERSION SUNW_1.1 {
    global:
	__xnet_bind	{ TYPE = FUNCTION };
	__xnet_connect	{ TYPE = FUNCTION };
	__xnet_getsockopt	{ TYPE = FUNCTION };
	__xnet_listen	{ TYPE = FUNCTION };
	__xnet_recvmsg	{ TYPE = FUNCTION };
	__xnet_sendmsg	{ TYPE = FUNCTION };
	__xnet_sendto	{ TYPE = FUNCTION };
	__xnet_socket	{ TYPE = FUNCTION };
	__xnet_socketpair	{ TYPE = FUNCTION };
} SUNW_0.7;

SYMBOL_VERSION SUNW_0.7 {
    global:
	endnetent	{ TYPE = FUNCTION };
	endprotoent	{ TYPE = FUNCTION };
	endservent	{ TYPE = FUNCTION };
	ether_aton	{ TYPE = FUNCTION };
	ether_hostton	{ TYPE = FUNCTION };
	ether_line	{ TYPE = FUNCTION };
	ether_ntoa	{ TYPE = FUNCTION };
	ether_ntohost	{ TYPE = FUNCTION };
	getnetbyaddr	{ TYPE = FUNCTION };
	getnetbyaddr_r	{ TYPE = FUNCTION };
	getnetbyname	{ TYPE = FUNCTION };
	getnetbyname_r	{ TYPE = FUNCTION };
	getnetent	{ TYPE = FUNCTION };
	getnetent_r	{ TYPE = FUNCTION };
	getprotobyname_r	{ TYPE = FUNCTION };
	getprotobynumber_r	{ TYPE = FUNCTION };
	getprotoent_r	{ TYPE = FUNCTION };
	getservbyname_r	{ TYPE = FUNCTION };
	getservbyport_r	{ TYPE = FUNCTION };
	getservent	{ TYPE = FUNCTION };
	getservent_r	{ TYPE = FUNCTION };
	htonl			{ TYPE = FUNCTION };
	htons			{ TYPE = FUNCTION };
	ntohl			{ TYPE = FUNCTION };
	ntohs			{ TYPE = FUNCTION };
	setnetent	{ TYPE = FUNCTION };
	setprotoent	{ TYPE = FUNCTION };
	setservent	{ TYPE = FUNCTION };
	socketpair	{ TYPE = FUNCTION };

	# For ABI reasons, the following symbols are in SISCD_2.3 on 32-bit
	# sparc, which is inherited by SUNW_0.7. On all other platforms,
	# they are simply in SUNW_0.7
$if _sparc && _ELF32
} SISCD_2.3;

SYMBOL_VERSION SISCD_2.3 {
    global:
$endif
	accept	{ TYPE = FUNCTION };
	bind	{ TYPE = FUNCTION };
	connect	{ TYPE = FUNCTION };
	getpeername	{ TYPE = FUNCTION };
	getprotobyname	{ TYPE = FUNCTION };
	getprotobynumber	{ TYPE = FUNCTION };
	getprotoent	{ TYPE = FUNCTION };
	getservbyname	{ TYPE = FUNCTION };
	getservbyport	{ TYPE = FUNCTION };
	getsockname	{ TYPE = FUNCTION };
	getsockopt	{ TYPE = FUNCTION };
	inet_lnaof	{ TYPE = FUNCTION };
	inet_makeaddr	{ TYPE = FUNCTION };
	inet_network	{ TYPE = FUNCTION };
	listen	{ TYPE = FUNCTION };
	recv	{ TYPE = FUNCTION };
	recvfrom	{ TYPE = FUNCTION };
	recvmsg	{ TYPE = FUNCTION };
	send	{ TYPE = FUNCTION };
	sendmsg	{ TYPE = FUNCTION };
	sendto	{ TYPE = FUNCTION };
	setsockopt	{ TYPE = FUNCTION };
	shutdown	{ TYPE = FUNCTION };
	socket	{ TYPE = FUNCTION };
};

# There really should be only one SUNWprivate version.
# Don't add any more.  Add new private symbols to SUNWprivate_1.3

SYMBOL_VERSION SUNWprivate_1.3 {
    global:
	_link_aton	{ TYPE = FUNCTION };
	_link_ntoa	{ TYPE = FUNCTION };
	_nss_initf_ethers	{ TYPE = FUNCTION };
	_nss_initf_net	{ TYPE = FUNCTION };
	_nss_initf_netmasks	{ TYPE = FUNCTION };
	_nss_initf_proto	{ TYPE = FUNCTION };
	_nss_initf_services	{ TYPE = FUNCTION };
	getallifaddrs	{ TYPE = FUNCTION };
	getallifs	{ TYPE = FUNCTION };
	str2ether	{ TYPE = FUNCTION };
	str2addr	{ TYPE = FUNCTION };
	str2netent	{ TYPE = FUNCTION };
	str2protoent	{ TYPE = FUNCTION };
} SUNWprivate_1.2;

SYMBOL_VERSION SUNWprivate_1.2 {
    global:
	getnetmaskbyaddr	{ TYPE = FUNCTION };
	getnetmaskbynet		{ TYPE = FUNCTION };
} SUNWprivate_1.1;

SYMBOL_VERSION SUNWprivate_1.1 {
    global:
	bindresvport	{ TYPE = FUNCTION };
	bootparams_getbyname	{ TYPE = FUNCTION };
	_ruserpass	{ TYPE = FUNCTION };
    local:
	*;
};
